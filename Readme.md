# BackEnd/Api Technical SDET Assesment 

## To run the API, clone the project and run the following commands
### Pre Reqs -> Docker installed and running on local machine

```bash
docker build -t "wh-json-server:dockerfile" .
docker run -p 3000:3000 -v `pwd`:/data wh-json-server:dockerfile --watch WH.json
```

To check the API is up and running, navigate to http://localhost:3000

Further information is provided in the json-server documentation -> https://github.com/typicode/json-server